class Code
  attr_reader :pegs

  PEGS = {
    'r' => 'red',
    'g' => 'green',
    'b' => 'blue',
    'y' => 'yellow',
    'o' => 'orange',
    'p' => 'purple'
  }.freeze

  def initialize(pegs)
    @pegs = pegs
  end

  # factory method
  def self.parse(string)
    arr = string.downcase.chars
    arr.each do |ch|
      raise 'parse error' unless PEGS.key?(ch)
    end
    Code.new(arr)
  end

  # factory method
  def self.random
    arr = []
    4.times { arr << PEGS.keys.sample(1) }
    Code.new(arr.flatten)
  end

  def [](idx)
    pegs[idx]
  end

  def ==(code)
    return false unless code.is_a? Code
    pegs == code.pegs ? true : false
  end

  def exact_matches(code)
    matches = 0
    pegs.each_index do |idx|
      matches += 1 if pegs[idx] == code.pegs[idx]
    end
    matches
  end

  def near_matches(code)
    matches = code.pegs.uniq.join.count(pegs.join)
    pegs.each_index do |idx|
      matches -= 1 if pegs[idx] == code.pegs[idx]
    end
    matches
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts 'enter a guess:'
    guess = $stdin.gets.chomp.downcase
    Code.parse(guess)
  end

  def display_matches(code)
    print "exact Matches: #{secret_code.exact_matches(code)}\n"
    print "near Matches: #{secret_code.near_matches(code)}\n"
  end

  def play
    guesses = 10
    until guesses.zero?
      guess = get_guess
      break if won?(guess)
      display_matches(guess)
      guesses -= 1
    end
    puts 'you have won or guesses are up conditional needed'
  end

  def won?(code)
    code.pegs == secret_code.pegs ? true : false
  end
end
